---
Title:	STM32 Smart Connector   
Author: Vincent for Everysens   
Date:	Dec, 2020
---  

# STM32 Smart Connector
    
version : **1.0**     

Smart connector is a second layer over ST-Link V2 in order to multiplex UART and SWD lines of STM32 ARM Cortex M0 microcontrollers (STM32L0) for flashing and debugging. 

## Usage

Not yet written
