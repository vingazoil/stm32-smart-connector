/**
 * @vincent
 * 04/11/2020
 */


#define VERSION               "0.9"
#define PRINT                 true      // print (or not) informational messages
#define PIN_FREQ_SENSING      2         // physical digital pin 
#define PIN_RESET_SENSING     3         // physical digital pin 
#define PIN_TOGGLE_MODE       4         // physical digital pin 
#define FREQ_SENSING_SAMPLE   1000      // how much samples to detect frequency (1000 sample = 4ms on Arduino UNO)
#define FLASHING_TIMEOUT      40000     // = 40 seconds



/**
 * Globals variables
 */
static bool prevFreqSensed, prevResetSensed;
static unsigned long timeBeginFlashing, timeLastPoint;



/**
 * General Functions 
 */
 
void print(char * s)
{
  #if defined PRINT && PRINT == true
  Serial.print(s);
  #endif
}


bool frequencySensing()
{
  unsigned long low = 0, high = 0;
  long i = FREQ_SENSING_SAMPLE;
  while(i--){  
    if(digitalRead(PIN_FREQ_SENSING) == HIGH)
      high++;
    else
      low++;
  }  
  return (low > (FREQ_SENSING_SAMPLE >> 2)); // (divide by 4) => 1/4 of samples should be level different than others to affirm it's a Freq|PWM
}


bool resetSensing()
{
  unsigned long low = 0, high = 0;
  long i = 20; // = 2 milliseconds + 20 sampling to evaluate RESET state. 
  while(i--){  
    delayMicroseconds(100);
    if(digitalRead(PIN_RESET_SENSING) == HIGH)
      high++;
    else
      low++;
  }  
  return (low > high); // more low than high = reset !
}


void boot() 
{    
  print("\n");
  print("   _____   ___________  ___________  ______\n");
  print("  / __/ | / / __/ _ \\ \\/ / __/ __/ |/ / __/\n");
  print(" / _/ | |/ / _// , _/\\  /\\ \\/ _//    /\\ \\\n");
  print("/___/ |___/___/_/|_| /_/___/___/_/|_/___/\n");
  print("\n");
  print("STM32 Smart Connector\n");
  print("version ");
  print(VERSION);
  print("\n");
}


/**
 * Line Mode
 */

typedef enum {
  LINE_MODE_FTDI = 0,   // FTDI USB UART (Rx/Tx lines)
  LINE_MODE_SWD,        // ST-Link SWD (Data/Clock lines)
} LINE_MODE;


void toggleLineMode(int mode)
{
  if(mode == LINE_MODE_FTDI){
    digitalWrite(PIN_TOGGLE_MODE, LOW);    
  }
  if(mode == LINE_MODE_SWD){
    digitalWrite(PIN_TOGGLE_MODE, HIGH);
  }
}


/**
 * Main State Machine
 */

typedef enum {
  STEP_BOOT = 0,
  STEP_RESET_SENSING_INITIAL,  
  STEP_RESET_SENSING_INITIAL_WAIT, 
  STEP_RESET_SENSING_TERMINAL, 
  STEP_RESET_SENSING_TERMINAL_WAIT,  
  STEP_FREQ_SENSING,  
  STEP_FLASHING_ENDED,    
} STEP;


int machine(int step)
{

  if(step == STEP_BOOT)
  {
    boot();  
    return step = STEP_RESET_SENSING_INITIAL;
  }


  if(step == STEP_RESET_SENSING_INITIAL)
  {    
    print("\n> wait for reset\n");   
    return step = STEP_RESET_SENSING_INITIAL_WAIT;
  }


  if(step == STEP_RESET_SENSING_INITIAL_WAIT)
  {    
    if(resetSensing()){
      print("> reset detected !\n");
      return step = STEP_FREQ_SENSING;
    }
  }  


  if(step == STEP_FREQ_SENSING)
  {
    if(frequencySensing()){
      toggleLineMode(LINE_MODE_SWD); // immediately toggle to SWD line mode
      print("> frequency detected !\n");
      print("> flashing begins ");      
      timeBeginFlashing = timeLastPoint = millis();      
      return step = STEP_RESET_SENSING_TERMINAL; // go wait terminal reset
    }
    print("> error : no frequency detected\n");
    return step = STEP_RESET_SENSING_INITIAL; // return to wait for initial reset
  }  


  if(step == STEP_RESET_SENSING_TERMINAL)
  {
    delay(50); // because initial reset last about ~40ms, we must wait it finish before waiting the terminal one.
    return step = STEP_RESET_SENSING_TERMINAL_WAIT;
  }

  if(step == STEP_RESET_SENSING_TERMINAL_WAIT)
  {
    if((millis() - timeBeginFlashing) > FLASHING_TIMEOUT){
      print("\n> error : timeout\n");
      return step = STEP_RESET_SENSING_INITIAL; // return to wait for initial reset
    }
    
    if(millis() - timeLastPoint > 1000){
      timeLastPoint = millis(); 
      print("."); 
    }

    if(resetSensing()){
      delay(10); // little delay after terminal reset then toggle line mode
      toggleLineMode(LINE_MODE_FTDI);

      print("\n"); // for next message show correctly
      return step = STEP_FLASHING_ENDED;
    }
  }


  if(step == STEP_FLASHING_ENDED)
  {    
    print("> flashing successfully ended !\n");
    return step = STEP_RESET_SENSING_INITIAL;
  }


  return step;
}


/**
 * Arduino Callbacks
 */

void setup() 
{  
  Serial.begin(115200);  
  pinMode(PIN_FREQ_SENSING,  INPUT_PULLUP);  
  pinMode(PIN_RESET_SENSING, INPUT_PULLUP);    
  pinMode(PIN_TOGGLE_MODE,   OUTPUT);    
}

void loop() 
{
  static STEP step = STEP_BOOT;
  step = machine(step);  
}
